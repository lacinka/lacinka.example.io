---
layout: default
---

<div class="posts">
  {% for p in site.pages %}
    <article class="post"><h1><a href="{{ site.baseurl }}{{ p.url }}">{{ site.data.titles[p.id] }}</a></h1>
      {% assign author = site.data.authors[p.id] %}
      <div class="date">
        <img src='{{site.data.authordata[author]["pic"]}}' height="15rem;"> <em>{{ site.data.authordata[author]["readablename"] }} ({{ site.data.authors[p.id] }}) {{ site.data.dates[p.id] | date: "%B %e, %Y" }} u {{ site.data.times[p.id]}}</em>
      </div>
      <div class="entry">{{ p.excerpt }}</div><a href="{{ site.baseurl }}{{ p.url }}" class="read-more">Čytać daliej</a></article>
  {% endfor %}
</div>